<!-- Homework 3 skeleton -->
<html>
<body>
<?php
  echo "Homework 3<br/>";
  /* Display your name */
  echo "Name<br/>";
  $firstname = 'David';
  $lastname = 'Mapa';
  //echo $firstname . " " . $lastname ." or ";  	/* Method 1.a */
  echo "$firstname $lastname<br/><br/>";		/* Method 1.b */
  
  echo "Read the first 5 lines of text data from the file hw3.txt.<br/>Remove the first three characters of each line read and print the rest of the line.<br/><br/>";
  
  /* Assign file name to infile variable */
  $infile = "hw3.txt";

  $fp = fopen($infile,'r'); //Open file for reading
  
  if(!$fp){ // Test if the file cannot be opened
	  echo "<p> File <b>$infile</b> cannot be opened.</p>";
	  exit;
  }
  
  $readline = fgets($fp);   						// Read 1st line from the file
  $readline = substr($readline,3); 					// Remove the first 3 characters from each read line
  echo "1 - Line read/adjusted: $readline <br/>";   // Print the 1st read/adjusted line
  
  $readline = fgets($fp);   						// Read 2nd line from the file
  $readline = substr($readline,3); 					// Remove the first 3 characters from each read line
  echo "2 - Line read/adjusted: $readline <br/>";   // Print the 2nd read/adjusted line
  
  $readline = fgets($fp);   						// Read 3rd line from the file
  $readline = substr($readline,3); 					// Remove the first 3 characters from each read line
  echo "3 - Line read/adjusted: $readline <br/>";   // Print the 3rd read/adjusted line
	 
  $readline = fgets($fp);   						// Read 4th line from the file
  $readline = substr($readline,3); 					// Remove the first 3 characters from each read line
  echo "4 - Line read/adjusted: $readline <br/>";   // Print the 4th read/adjusted line	 
 
  $readline = fgets($fp);   						// Read 5th line from the file
  $readline = substr($readline,3); 					// Remove the first 3 characters from each read line
  echo "5 - Line read/adjusted: $readline <br/>";   // Print the 5th read/adjusted line
  
  fclose($fp);  // Close file
  exit;
?>
</body>
</html>